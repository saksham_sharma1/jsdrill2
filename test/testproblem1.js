import inventory from "../inventory.js"
import solve from "../problem1.js"

const out =solve(inventory);

if(out.length>0)
{
    const car33=out[0]
    console.log(`Car33 is ${car33.car_year}  ${car33.car_make}  ${car33.car_model}`);
}
else{
    console.log("Car33 not found");
}